// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app =angular.module('starter', ['ngCookies','ionic', 'starter.controllers','ngOpenFB','starter.directives'])

//////

app.run(function ($rootScope, $cookieStore, $state, ngFB) {
	ngFB.init({appId: '953225268071296'});
    // Check login session
   // $rootScope.$on('$stateChangeStart', function (event, next, current) {
        var userInfo = $cookieStore.get('userInfo');
      // if (!userInfo) {
            // user not logged in | redirect to login
      //      if (next.name !== "start") {
                // not going to #welcome, we should redirect now
      //         event.preventDefault();
      //          $state.go('app.start');
      //      }
      //  } else if (next.name === "start") {
      //      event.preventDefault();
      //      $state.go('login');
      //  }
   // });
});

//////

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

  

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
    url: '/browse',
    views: {
      'menuContent': {
        templateUrl: 'templates/browse.html'
      }
    }
  })
  .state('app.maps', {
    url: '/maps',
    views: {
      'menuContent': {
        templateUrl: 'templates/maps.html',
        controller: 'MapsCtrl'
      }
    }
  })
  
  .state('app.leaderboard', {
    url: '/leaderboard/:board',
    views: {
      'menuContent': {
        templateUrl: 'templates/leaderboards.html',
        controller: 'LeaderboardCtrl'
      }
    }
  })

  .state('app.myhunts', {
    url: '/myhunts',
    views: {
      'menuContent': {
        templateUrl: 'templates/myhunts.html',
        controller: 'MyhuntsCtrl'
      }
    }
  })

  .state('app.start', {
    url: '/start',
    views: {
      'menuContent': {
        templateUrl: 'templates/start.html',
        controller: 'StartCtrl'
      }
    }
  })
  
   .state('app.landing', {
    url: '/landing',
    views: {
      'menuContent': {
        templateUrl: 'templates/landing.html',
        controller: 'LandingCtrl'
      }
    }
  })

  .state('app.company_landing', {
    url: '/landing/:companyId',
    views: {
      'menuContent': {
        templateUrl: 'templates/company_landing.html',
        controller: 'CompanyLandingCtrl'
      }
    }
  })

  .state('app.hunt', {
    url: '/hunt/:huntId',
    views: {
      'menuContent': {
        templateUrl: 'templates/hunt.html',
        controller: 'HuntCtrl'
      }
    }
  })
 
   .state('app.profile',{
	   url: '/profile',
	   views: {
		   'menuContent': {
			   templateUrl: 'templates/profile.html',
			   controller: 'ProfileCtrl'
		   }
	   }
	   
   })
  
  
 

  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/start');
});
