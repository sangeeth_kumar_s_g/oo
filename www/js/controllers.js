var app = angular.module('starter.controllers', ['ionic', 'ngCordovaOauth', 'ngOpenFB', 'ngCordova'])

app.controller('AppCtrl', function($scope, $ionicModal, $http, $timeout, $location, $cookieStore, ngFB) {

    // oauthApp.controller('welcomeCtrl', function ($scope, $state, $cookieStore) {	

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});
    var userInfo = $cookieStore.get('userInfo');
    var loggedin = 1;
    var userData = {};
    var uid;
    var accessToken;


    $scope.currentColorScheme = {
        primaryBackground: "blue"
    };
    $scope.appTitle = "Orion Orbit";

    $scope.currentUser = "Nabil Enayet"
    $scope.currentUserImg = "img/current_user.jpg";

    // Form data for the login modal
    $scope.loginData = {};
    var user = {};

    // formdata for register modal
    $scope.registerData = {};
    //$scope.fblogin = function (){
    //	  alert('Login With Facebook');
    // }
    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        id: 'login',
        scope: $scope
    }).then(function(modal) {
        $scope.modal_login = modal;
    });

    $ionicModal.fromTemplateUrl('templates/register.html', {
        id: 'register',
        scope: $scope
    }).then(function(modal) {
        $scope.modal_register = modal;
    });

    $ionicModal.fromTemplateUrl('templates/profile.html', {
        id: 'profile',
        scope: $scope
    }).then(function(modal) {
        $scope.modal_profile = modal;
    });



    $ionicModal.fromTemplateUrl('templates/signup.html', {
        id: 'signup',
        scope: $scope
    }).then(function(modal) {
        $scope.modal_signup = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {




        $scope.modal_login.hide();
    };
    $scope.closeSignUp = function() {
        $scope.modal_signup.hide();
    };

    $scope.closeProfile = function() {
        $scope.modal_profile.hide();
    };
    // Open the login modal
    $scope.login = function() {
        $scope.modal_login.show();
    };
    $scope.signUp = function() {
        $scope.modal_login.hide();
        $scope.modal_signup.show();
    };
    $scope.profile = function() {
        $scope.modal_register.hide();
        $scope.modal_profile.show();
        $scope.user = $cookieStore.get('userInfo');
    };

    $scope.register = function() {
        $scope.modal_login.hide();
        $scope.modal_register.show();
    };
    $scope.closeRegister = function() {
        $scope.modal_register.hide();
    };

    $scope.updateProfile = function() {
        /*
        var formData = {

            body: {


                uId: user.id,
                accessToken: $scope.accessToken,
                displayName: user.displayName,
                email: user.email,
                gender: user.gender,
                location: user.location,
                profilePic: user.profilePic

            }


        };
        alert("Profile details:" + JSON.stringify(formData));
        //sending fblogin data to server.Madhu's system in network
        var res = $http.post('http://192.168.1.147:8080/o2core/services/fblogin', formData, {

            headers: {

                'Content-Type': 'application/json'
            }

        });
        res.success(function(data, status, config) {
            //$scope.message = data;
            alert("Success" + status);
        });
        res.error(function(data, status, config) {
            alert("failure message: " + JSON.stringify({
                status: status
            }));
        });
*/
        $scope.user = $cookieStore.put('userInfo');
        console.log('userInfo', user);
        $scope.modal_profile.hide();
        alert("info:" + JSON.stringify(user));
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        alert("Login with o2 credentials");
/*
        var formData = {

            body: {
                userName: $scope.loginData.username,
                userPassword: $scope.loginData.password
            }


        };
        alert(JSON.stringify(formData));
        //sending login data to server.Ashish's system in network
        var res = $http.post('http://192.168.1.105:8080/o2core/services/login', formData, {

            headers: {

                'Content-Type': 'application/json'
            }

        });
        res.success(function(data, status, config) {
            //$scope.message = data;
            alert("Success" + status);
        });
        res.error(function(data, status, config) {
            alert("failure message: " + JSON.stringify({
                status: status
            }));
        });
        // Making the fields empty
        //
*/
        console.log('Doing login', $scope.loginData);
        $scope.loginData.username = '';
        $scope.loginData.password = '';

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);
    };

    ////////////////////////////////
    $scope.doRegister = function() {
        alert("data is :-" + JSON.stringify($scope.registerData));

        var formData = {

            body: {
                firstName: $scope.registerData.firstName,
                emailId: $scope.registerData.emailId,
                password: $scope.registerData.password
            }


        };
        
        /*
        alert(JSON.stringify(formData));
        //sending sign up data to server.Ashish's system in network
        var res = $http.post('http://192.168.1.105:8080/o2core/services/signup', formData, {

            headers: {

                'Content-Type': 'application/json'
            }

        });
        res.success(function(data, status, config) {
            //$scope.message = data;
            alert("Success" + status);
        });
        res.error(function(data, status, config) {
            alert("failure message: " + JSON.stringify({
                status: status
            }));
        });
        // Making the fields empty
        //

        console.log('Doing Register', $scope.registerData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeRegister();
        }, 1000);
        */
    };

    ////////////////////////////////


    // FB Login
    
    $scope.fblogin = function() {
        
        ngFB.login({
            scope: 'email,publish_actions'
        }).then(
            function(response) {
                if (response.status === 'connected') {
                    console.log('Facebook login succeeded');
                    getUserInfo();
                    $scope.closeLogin();
                } else {
                    alert('Facebook login failed');
                }
            });

        function getUserInfo() {
            ngFB.api({
                path: '/me',
                params: {
                    fields: 'id,name,birthday,email,location,picture,gender'
                }
            }).then(
                function(response) {
                    console.log('Facebook Login RESPONSE: ' + angular.toJson(response));
                    console.log(response);
                    console.log('Successful login for: ' + response.name);

                    user.id = response.id;
                    user.displayName = response.name;
                    user.email = response.email;
                    user.bio = response.bio;
                    user.gender = response.gender;
                    user.likes = response.user_likes;
                    user.birthday = response.birthday;
                    user.location = response.location.name;
                    user.profilePic = response.picture.data.url;
                    $cookieStore.put('userInfo', user);
                    userData = user;
                    console.log('userInfo', user);
                    $scope.closeSignUp();
                    $scope.profile();
                },
                function(error) {
                    alert('Facebook error: ' + error.error_description);
                });
        }
    };
        /*
    $scope.fblogin =function () {
        alert("FB login");
        FB.login(function (response) {
            alert("response : "+JSON.stringify(response));
            if (response.authResponse) {
                alert("response success");
            uid = response.authResponse.userID;
            accessToken = response.authResponse.accessToken;
                console.log('uid: '+uid, 'accesstoken: ' +accessToken);
                getUserInfo();
				loggedin=2;
            } else {
                alert("response failure");
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {scope: 'email,user_photos,user_videos'});

        function getUserInfo() {
            // get basic info
           
        
				FB.api('/me','GET', {fields:'id,name,birthday,email,location,picture,gender'}, function(response) {
					     console.log('Facebook Login RESPONSE: ' + angular.toJson(response));
	                     console.log(response);
                    
                         console.log('Successful login for: ' + response.name);
						  
                           user.id = response.id;
                           user.displayName = response.name;
                           user.email = response.email;
					       user.bio = response.bio;
					       user.gender= response.gender;
					       user.likes= response.user_likes;
						   user.birthday =response.birthday;
						   user.location= response.location.name;
                           user.profilePic = response.picture.data.url;
                           $cookieStore.put('userInfo',user);
						   userData = user;		
						   console.log('userInfo',user);
                           $scope.closeSignUp();
						   $scope.profile();
                         //document.getElementById('status').innerHTML =
                        // 'Thanks for logging in, ' + response.name + '! <br> Name: ' + response.name + '<br> Email: ' + response.email + '<br> Gender: '+ response.gender;
                   });
				
                // get profile picture
             //  FB.api('/me/picture?type=normal', function (picResponse) {
             //       console.log('Facebook Login RESPONSE: ' + picResponse.data.url);
                   // response.imageUrl = picResponse.data.url;
			//		$scope.user.profilePic = picResponse.data.url;
                    // store data to DB - Call to API
                    // Todo
                    // After posting user data to server successfully store user data locally
                    

            //    });
            
        }
		 
    };

*/
    // Google Plus Login
    $scope.gplusLogin = function() {
        var myParams = {
            // Replace client id with yours
            'clientid': '498544449285-1bhjf86h06av2md0d6qpud3ghsiamos3.apps.googleusercontent.com',
            'cookiepolicy': 'single_host_origin',
            'callback': loginCallback,
            'approvalprompt': 'force',
            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.login'
        };
        gapi.auth.signIn(myParams);

        function loginCallback(result) {
            if (result['status']['signed_in']) {
                var request = gapi.client.plus.people.get({
                    'userId': 'me'
                });
                request.execute(function(resp) {
                    console.log('Google+ Login RESPONSE: ' + angular.toJson(resp));
                    var userEmail;
                    if (resp['emails']) {
                        for (var i = 0; i < resp['emails'].length; i++) {
                            if (resp['emails'][i]['type'] == 'account') {
                                userEmail = resp['emails'][i]['value'];
                            }
                        }
                    }
                    // store data to DB
                    // var user = {};
                    user.displayName = resp.displayName;
                    user.email = userEmail;
                    user.givenName = resp.givenName;
                    user.url = resp.url;
                    user.gender = resp.gender;
                    user.profilePic = resp.image.url;
                    $cookieStore.put('userInfo', user);
                    console.log('userInfo', user);
                    console.log('userInfo', user);
                    $scope.closeSignUp();
                    $scope.profile();
                });
            }
        }
    };
    /*
    $scope.gplusLogin = function () {
    $cordovaOauth.google("498544449285-1bhjf86h06av2md0d6qpud3ghsiamos3.apps.googleusercontent.com", ["email"]).then(function(result) {
    console.log("Response Object -> " + JSON.stringify(result));
     }, function(error) {
    console.log("Error -> " + error);
      });
    };
    
     */

});



app.controller('ProfileCtrl', function($scope, $window, $state, $cookieStore) {
    // Set user details
    alert('profile');
    $scope.user = $cookieStore.get('userInfo');

    // Logout user
    $scope.logout = function() {
        $cookieStore.remove("userInfo");
        $scope.login();
        $window.location.reload();
    };
})



app.controller('StartCtrl', function($scope) {
    $scope.testData = [{
        name: 'test1'
    }, {
        name: 'test2'
    }];
})

app.controller('MapsCtrl', function($scope, $ionicLoading) {
  $scope.mapCreated = function(map) {
    $scope.map = map;
      console.log("mapping");
  };

  $scope.centerOnMe = function () {
    console.log("Centering");
    if (!$scope.map) {
      return;
    }

    $scope.loading = $ionicLoading.show({
      content: 'Getting current location...',
      showBackdrop: false
    });

    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log('Got pos', pos);
      $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
    var marker = new google.maps.Marker({position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),map: $scope.map, icon:    iconBase + 'placemark_circle_highlight.png'});
      $ionicLoading.hide()
    }, function (error) {
      alert('Unable to get location: ' + error.message);
    });
  };
})


app.controller('LandingCtrl', function($scope, $http) {
    
    $scope.limitItemsTo = 6;

  $scope.showMoreItems = function() {
    setTimeout(function(){ 
      $scope.limitItemsTo += 4; 
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }, 3000);
  }


   $scope.clients = [
    {name: 'goibibo.com', id:0,   imgUrl:'img/goibibo.jpg'},
    {name: 'Thomas Cook', id:1,   imgUrl:'img/cook.png'},
    {name: 'Infosys',     id:2,   imgUrl:'img/infosys.png'},
    {name: 'Welingkar',   id:3,   imgUrl:'img/welingkar.jpg'},
    {name: 'Infosys',     id:4,   imgUrl:'img/infosys.png'},
    {name: 'Thomas Cook', id:1,   imgUrl:'img/cook.png'},
    {name: 'Infosys',     id:2,   imgUrl:'img/infosys.png'},
    {name: 'Infosys',     id:4,   imgUrl:'img/infosys.png'},
    {name: 'Thomas Cook', id:1,   imgUrl:'img/cook.png'},
    {name: 'Infosys',     id:2,   imgUrl:'img/infosys.png'},
    {name: 'Welingkar',   id:3,   imgUrl:'img/welingkar.jpg'},
    {name: 'Infosys',     id:4,   imgUrl:'img/infosys.png'},
    {name: 'Thomas Cook', id:1,   imgUrl:'img/cook.png'},
    {name: 'Infosys',     id:2,   imgUrl:'img/infosys.png'},
    {name: 'Infosys',     id:4,   imgUrl:'img/infosys.png'},
    {name: 'Welingkar',   id:3,   imgUrl:'img/welingkar.jpg'},
    {name: 'Infosys',     id:4,   imgUrl:'img/infosys.png'},
    {name: 'Welingkar',   id:5,   imgUrl:'img/welingkar.jpg'}
  ];

    /*
    //Getting corporate list from Ashish
    $http.get('http://192.168.1.127:8080/o2core/services/CorpDetails')
            .success(function (data, status, headers, config) {
                $scope.corporateDetails = data.body.corporates;
                  alert("sucess" + status);
                  //alert('data is: '+JSON.stringify($scope.corporateDetails));
                     console.log($scope.corporateDetails);
            })
            .error(function (data, status, header, config) {
               alert(status);
            });
        
      */



})

app.controller('CompanyLandingCtrl', function($scope, $stateParams, $http, $cordovaNetwork) {
//app.controller('CompanyLandingCtrl', function($scope, $stateParams, $http) {
    $scope.clients = [{
            name: 'goibibo.com',
            imgUrl: 'img/goibibo.jpg',
            description: 'Welcome to Goibibo treasure hunts powered by orion orbit. Goibibo has always strived to make your travel experience amazing. This is another initiative by us where you can you explore new places through a treasure hunt',
            hunts: [{
                name: 'Search',
                locked: false,
                id: 0129383
            }, {
                name: 'Mysuru',
                locked: false,
                id: 0129382
            }, {
                name: 'Bombay',
                locked: true,
                id: 0129381
            }, {
                name: 'Delhi',
                locked: true,
                id: 0129380
            }]
        },

        {
            name: 'Thomas Cook',
            imgUrl: 'img/cook.png'
        }, {
            name: 'Infosys',
            imgUrl: 'img/infosys.png'
        }, {
            name: 'Welingkar',
            imgUrl: 'img/welingkar.jpg'
        }, {
            name: 'Infosys',
            imgUrl: 'img/infosys.png'
        }, {
            name: 'Welingkar',
            imgUrl: 'img/welingkar.jpg'
        }
    ];

    $scope.currentClient = $scope.clients[$stateParams.companyId];




    //Network test 

    $scope.networkTest = function(){


    $scope.NetworkType = $cordovaNetwork.getNetwork()
    //     $scope.isOnline = $cordovaNetwork.isOnline()
    //    $scope.isOffline = $cordovaNetwork.isOffline()



    alert ('You are on : '+$scope.NetworkType);
        if($scope.NetworkType!=wifi)
        {
        alert("You are not on Wifi. Better connect to wifi to continue. Game data need to be downloaded");
        }

    };


})

app.controller('MyhuntsCtrl', function($scope, $stateParams) {

  $scope.savedHunts = [
    {
      name: "Goibibo Search Hunt",
      icon: "ion-location",
      id: "0129383"
    },
    {
      name: "Infosys Delhi Hunt",
      icon: "ion-location",
      id: "0129383"
    },
    {
      name: "Infosys Search Hunt",
      icon: "ion-clock",
      id: "0129383"
    },
  ];

  $scope.completedHunts = [
    {
      name: "Infosys Search Hunt",
      score: "150",
      boardId: 0
    },
    {
      name: "Goibibo Search Hunt",
      score: "150",
      boardId: 0
    },
    {
      name: "Goibibo Bombay Hunt",
     score: "15",
      boardId: 2
    },
    {
      name: "Goibibo Delhi Hunt",
      score: "350",
      boardId: 1
    }
  ];

})


app.controller('LeaderboardCtrl', function($scope, $stateParams) {

  $scope.initializeBoard = $stateParams.board;
  $scope.current_hunt = $scope.initializeBoard;

  $scope.leaders_chosen_hunt = $scope.global_leaders;

  $scope.global_leaders = [
    {
      name:"Goibibo Search Hunt",
      users: [
        {
          img: "https://lh3.googleusercontent.com/-48TUHSykRyg/AAAAAAAAAAI/AAAAAAAAAAA/y0RU2VKLr94/s32-c-k-no/photo.jpg",
          name: "Karthik",
          score:"180"
        },
        {
          img: "https://lh5.googleusercontent.com/-ycHqXNzXgeI/AAAAAAAAAAI/AAAAAAAAAxY/XHUDRMoaMIE/s32-c-k-no/photo.jpg",
          name: "Peush",
          score:"75"
        },
        {
          img: "https://scontent-ord1-1.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/165312_10150114140872526_6560140_n.jpg?oh=5793b9b7153c61cc05a05daf2bbc9a63&oe=569D91DD",
          name: "Arvind",
          score:"10"
        }
      ]
    },
    {
      name:"Goibibo Delhi Hunt",
      users: [
        {
          img: "https://lh3.googleusercontent.com/-48TUHSykRyg/AAAAAAAAAAI/AAAAAAAAAAA/y0RU2VKLr94/s32-c-k-no/photo.jpg",
          name: "Karthik",
          score:"180"
        },
        {
          img: "https://lh6.googleusercontent.com/-QIKZrevP97c/AAAAAAAAAAI/AAAAAAAAAEQ/ToFjHx8elDE/s32-c-k-no/photo.jpg",
          name: "Gokul",
          score:"140"
        },
        {
          img: "https://secure.gravatar.com/avatar/6e48e463e0bf68dc5db16c932a8427dd/?s=80&d=https%3A%2F%2Fwww.fastmail.com%2Fstatic%2Fhomepage%2Fimages%2Fdefault-avatar.png",
          name: "Nabil",
          score:"160"
        },
        {
          img: "https://lh5.googleusercontent.com/-2xC091DJ3lY/AAAAAAAAAAI/AAAAAAAAACU/szCg_f2z5jA/s32-c-k-no/photo.jpg",
          name: "Sangeeth",
          score:"100"
     },
        {
          img: "https://lh5.googleusercontent.com/-ycHqXNzXgeI/AAAAAAAAAAI/AAAAAAAAAxY/XHUDRMoaMIE/s32-c-k-no/photo.jpg",
          name: "Peush",
          score:"75"
        },
        {
         img: "https://scontent-ord1-1.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/165312_10150114140872526_6560140_n.jpg?oh=5793b9b7153c61cc05a05daf2bbc9a63&oe=569D91DD",
          name: "Arvind",
          score:"10"
        }
      ]
    },
    {
      name:"Goibibo Bombay Hunt",
      users: [
        {
          img: "https://lh3.googleusercontent.com/-48TUHSykRyg/AAAAAAAAAAI/AAAAAAAAAAA/y0RU2VKLr94/s32-c-k-no/photo.jpg",
          name: "Karthik",
          score:"180"
        },
        {
          img: "https://lh6.googleusercontent.com/-QIKZrevP97c/AAAAAAAAAAI/AAAAAAAAAEQ/ToFjHx8elDE/s32-c-k-no/photo.jpg",
          name: "Gokul",
          score:"140"
        },
        {
          img: "https://lh5.googleusercontent.com/-2xC091DJ3lY/AAAAAAAAAAI/AAAAAAAAACU/szCg_f2z5jA/s32-c-k-no/photo.jpg",
          name: "Sangeeth",
          score:"100"
        },
        {
          img: "https://lh5.googleusercontent.com/-ycHqXNzXgeI/AAAAAAAAAAI/AAAAAAAAAxY/XHUDRMoaMIE/s32-c-k-no/photo.jpg",
          name: "Peush",
          score:"75"
        },
        {
          img: "https://scontent-ord1-1.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/165312_10150114140872526_6560140_n.jpg?oh=5793b9b7153c61cc05a05daf2bbc9a63&oe=569D91DD",
          name: "Arvind",
          score:"10"
        }
      ]
    },
  ];
  $scope.local_leaders = {};

})


app.controller('HuntCtrl', function($scope, $stateParams, $interval, $ionicPopup, $ionicActionSheet, $timeout, $cordovaBarcodeScanner, $cordovaCamera, $ionicModal, $http) {
    //app.controller('HuntCtrl', function($scope,$stateParams, $interval, $ionicPopup, $ionicActionSheet, $ionicModal, $http, $timeout) {
  
  $scope.animationControl = "animated fadeInRight"

  
    
    $scope.currentId = $stateParams.huntId;
    $scope.currentId = $stateParams.huntId;
    $scope.hunt_current = 1;
    $scope.QRcodes = [];
    var index = 0;
    $scope.easier_current = 1;
    $scope.QRcode = '';
    $scope.huntScore = 0;
    // $scope.selfiePic = "img/current_user.jpg";
    $scope.runTimer = true;
    //$scope.qrCodes = function () {


    //} 
   


    $ionicModal.fromTemplateUrl('templates/image_popup_modal.html', {
        id: 'image',
        scope: $scope
    }).then(function(modal) {
        $scope.modal_image = modal;
    });

    $scope.hunt_meta = {
        title: 'Search Hunt',
        timePerQuestion: 10
    };
    
    //Hunt integration step1 with Madhu
    /*
$scope.treasureHuntId = function()
  {
	  $scope.runTimer = false;
	  
	   var formData = { 
	    "body":{
			  "treasureHuntId" : 1 ,
			  "treasureHuntRouteId": 2
			
			  
		}
	   };
	   $http.post('http://192.168.1.147:8080/o2core/services/getTreasureHuntInfo',formData,
	   {
	   
	   headers: {
  
                          'Content-Type': 'application/json'
	   } 
	   })
            .success(function (data, status, headers, config) {
                
                  $scope.hunt_data = data.body.treasureHuntRouteList ;
                 alert('Status Code is: '+ status);
				 console.log($scope.hunt_data);
				 $scope.runTimer = true;
                    
            })
            .error(function (data, status, header, config) {
               alert("Error code :-" + status);
	  
  })};
 */

    
$scope.hunt_data = 
  [    
        {
            question:"Blooms once every 5-12 years, after the sun sets. Usually needs the chill and purity of the Himalayas, surprisingly in your backyard",
            answer:["flower","flowers","flowr"],
            points:50,
            time:10,
            type:"multiple",
            easierClues: [
                            {type:"image",data:"path/to/image.png",pointLoss:10},
                            {type:"text",data:"Starts with F",pointLoss:20}
                         ]
        }, 
        {
            question:"Capital of India",
            answer:["New Delhi"],
            points:100,
            time:-1,
            type:"multiple",
            easierClues: [
                            {type:"image",data:"path/to/image.png",pointLoss:30},
                            {type:"text",data:"Starts with F",pointLoss:60}
                         ]
        }, 
        {
            question:"Capital of India",
            answer:["flower","flowers","flowr"],
            points:50,
            time:100,
            type:"multiple",
            easierClues: [
                            {type:"image",data:"path/to/image.png",pointLoss:10},
                            {type:"text",data:"Starts with F",pointLoss:20}
                         ]
        }, 
        {
            question:"Capital of India",
            answer:["flower","flowers","flowr"],
            points:50,
            time:100,
            type:"multiple",
            easierClues: [
                            {type:"image",data:"path/to/image.png",pointLoss:10},
                            {type:"text",data:"Starts with F",pointLoss:20}
                         ]
        }, 
        {
            question:"Capital of India",
            answer:["flower","flowers","flowr"],
            points:50,
            time:100,
            type:"multiple",
            easierClues: [
                            {type:"image",data:"path/to/image.png",pointLoss:10},
                            {type:"text",data:"Starts with F",pointLoss:20}
                         ]
        }
  ];


  $scope.hunt_meta = {
    title:'Search Hunt',
    timePerQuestion: $scope.hunt_data[$scope.hunt_current-1].time
  }; 


    $scope.decrimentTimer = function() {
        if ($scope.hunt_meta.timePerQuestion > 0) {
            if ($scope.runTimer) {
                $scope.hunt_meta.timePerQuestion -= 1;
            }
            if ($scope.hunt_meta.timePerQuestion == 0) {
                $scope.showAlert("Out of time!", "You ran out of time, sorry! Better luck on the next one!", true);
            }
        }
    }

    $scope.nextQuestion = function() {
    $scope.animationControl = "";
    $scope.animationControl = "animated fadeOutLeft";

    setTimeout(function(){
      $scope.animationControl = "";
      $scope.animationControl = "animated fadeInRight";
    }, 200);
    $scope.hunt_current += 1;
    $scope.easier_current = 1;
    $scope.hunt_meta.timePerQuestion -= 1;
    $scope.hunt_meta.timePerQuestion = $scope.hunt_data[$scope.hunt_current-1].time;
  }
$scope.provideEasierClue = function() {
    $scope.hunt_data[$scope.hunt_current-1].points -= $scope.hunt_data[$scope.hunt_current-1].easierClues[$scope.easier_current-1].pointLoss;
    $scope.easier_current += 1;
  };

    $scope.finishHunt = function() {
        $scope.runTimer = false;
       var alertPopup = $ionicPopup.alert({
        title: "End of Hunt",
       template: "Congrats! Your score is " + $scope.huntScore
       });
      alertPopup.then(function(res) {
      $state.go('app.leaderboard', { "board": 0});
    });
    };

    $scope.showAlert = function(title, message, next) {
        $scope.runTimer = false;
        var alertPopup = $ionicPopup.alert({
            title: title,
            template: message
        });
        alertPopup.then(function(res) {
            if (next) {
                $scope.runTimer = true;
                $scope.nextQuestion();
            }
        });
    };

$scope.showPopupEasierClue = function() {
    $scope.runTimer = false;
    
    // An elaborate, custom popup
    var myPopup = $ionicPopup.show({
        title: 'Easier Clue?',
        subTitle: 'You can choose to play an easier clue for less points',
        scope: $scope,
        buttons: [
          { text: 'Cancel' ,
            onTap: function(e) {
              $scope.bonusPoints = true;
            }
          },
          {
            text: '<b>Easier Clue</b>',
            type: 'button-calm',
            onTap: function(e) {
              $scope.bonusPoints = false;
              $scope.nextQuestion();
            }
          }
        ]
    });
    myPopup.then(function(res) {
        if ($scope.bonusPoints) {
          $scope.show();
        } else {
          $scope.runTimer = true;
        }
    });
  };
    
    $scope.showPopupSubmit = function() {
        $scope.runTimer = false;
$scope.huntScore += $scope.hunt_data[$scope.hunt_current-1].points;
        // An elaborate, custom popup
        var myPopup = $ionicPopup.show({
            title: 'Correct!',
            subTitle: 'Choose to continue or try for bonus points!',
            scope: $scope,
            buttons: [{
                text: 'Bonus Points',
                onTap: function(e) {
                    $scope.bonusPoints = true;
                }
            }, {
                text: '<b>Next Place</b>',
                type: 'button-positive',
                onTap: function(e) {
                    $scope.nextQuestion();
                }
            }]
        });
        myPopup.then(function(res) {
            if ($scope.bonusPoints) {
                $scope.show();
            } else {
                $scope.runTimer = true;
            }
        });
    };


    $scope.show = function() {




        // Show the action sheet
        var hideSheet = $ionicActionSheet.show({
            scope: $scope,
            buttons: [{
                    text: '<i class="icon ion-android-camera pad-right"></i>Take a Selfie'
                }, {
                    text: '<i class="icon ion-ios-barcode-outline pad-right"></i>Scan Barcode'
                }, {
                    text: '<i class="icon ion-android-locate pad-right"></i>Geotag'
                }

            ],
            titleText: 'Select a Bonus Activity',
            cancelText: 'Cancel',
            cancel: function() {
                // add cancel code..
                $scope.runTimer = true;
            },
            buttonClicked: function(index) {
                console.log('BUTTON CLICKED', index);
                 if (index == 0) {
                    
                    $scope.takePicture();
                    // $scope.imgSrc = "data:image/jpeg;base64," + imageData;
                    $scope.modal_image.show();
                };
                if (index == 1) {
                    // alert('Barcode');
                    $scope.scanBarcode();
                };
                 if (index == 2) {
                     alert('Geotag');
                    $scope.geotag();
                };
                $scope.runTimer = true;
                return true;
            }
        });
        
        //Geotagging
  $scope.geotag = function() {
            navigator.geolocation.getCurrentPosition(function (pos) {
       var lat= pos.coords.latitude;
    var long = pos.coords.longitude;
    alert('Latitude: ' + lat + '\n' +'longitude: ' + long + '\n');
     });
       };
//QR code Function
        $scope.scanBarcode = function() {
            $scope.runTimer = false;
            $cordovaBarcodeScanner.scan().then(function(imageData) {
                    $scope.qrDataFound = [];

                    $scope.QRcode = imageData.text;
                    alert($scope.QRcode);
                    console.log("Barcode Format -> " + imageData.format);
                    console.log("Barcode text -> " + imageData.text);
                    console.log("Cancelled -> " + imageData.cancelled);
                
                
                //QR Code integration with Ashish
                /*
                    $http.get('http://192.168.1.127:8080/o2core/services/getQrDetails')
                        .success(function(data, status, headers, config) {
                            $scope.QRcodes = data.body.QrDetails;
                           // alert("sucess" + status);
                           // alert("UI text is " + $scope.QRcode);

                            $scope.dbText = $scope.QRcodes.map(function(text) {

                                return text.answerText;
                            });
                           // alert("database text are:-" + JSON.stringify($scope.dbText));

                            for (var i = 0; i < $scope.dbText.length; i++) {

                                if ($scope.dbText[i] == $scope.QRcode) {
                                    alert("Hey your answer is correct ..congrats!!");
                                    $scope.qrDataFound = $scope.dbText[i];
                                }




                            }
                            if ($scope.qrDataFound == '') {

                                alert("sorry yout answer is incorrect");
                            }




                        }, function(error) {
                            console.log("An error happened -> " + error);
                        });
                    console.log('QR from server : ', $scope.QRcodes);




                    //alert('data is: '+JSON.stringify($scope.corporateDetails));


                })
                .error(function(data, status, header, config) {
                    alert(status);
                });

*/

        });
        };
        
//CAMERA OPTIONS        
        var options = {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 100,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: true
        };

//Calling Camera
        $scope.takePicture = function() {
            alert('Selfie');
            $scope.runTimer = false;
            //  $scope.selfiePic = "img/current_user.jpg";

            //console.log('image :' +$scope.selfiePic);

            $cordovaCamera.getPicture(options).then(function(imageData) {
                //alert("data:image/jpeg;base64," + imageData);
                $scope.selfiePic = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                console.log(err);
            });

        };


        // For example's sake, hide the sheet after two seconds


    };

    $interval(function() {
        $scope.decrimentTimer();
    }, 1000);


})


;